const mix = require("laravel-mix");

mix.scripts(
    [
        "public/vendor/jquery-3.3.1.min.js",
        "public/vendor/bootstrap-3.3.7/js/bootstrap.min.js",
        "public/vendor/adminlte-2.4.5/js/adminlte.min.js"
    ],
    "public/js/app.js"
);

mix.styles(
    [
        "public/vendor/bootstrap-3.3.7/css/bootstrap.min.css",
        "public/vendor/adminlte-2.4.5/css/AdminLTE.min.css",
        "public/vendor/adminlte-2.4.5/css/skins/_all-skins.min.css"
    ],
    "public/css/app.css"
);
