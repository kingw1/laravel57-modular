<?php
Route::get('/', 'Core\Controllers\AuthController@index');
Route::get('login', 'Core\Controllers\AuthController@index')->name('login');
Route::post('login', 'Core\Controllers\AuthController@checkLogin')->name('login.check');

Route::group(['middleware' => 'auth'], function () {
    Route::get('logout', 'Core\Controllers\AuthController@logout')->name('logout');

    Route::group(['prefix' => 'admin'], function () {
        Route::get('module/module/{module}/submodules', 'Core\Controllers\ModuleController@getSubmoduleList')->name('admin.module.list.submodule');
        Route::resource('module/module', 'Core\Controllers\ModuleController', ['as' => 'admin']);
        Route::resource('module/group', 'Core\Controllers\ModuleGroupController', ['as' => 'admin.module']);
    });

    Route::get('/', 'Core\Controllers\DashboardController@index');

});
