<?php

return [
    'name' => 'Eastern App',
    'title' => 'Eastern App',
    'author' => 'Jndweb',
    'keywords' => 'Eastern App',
    'description' => 'Eastern App Inspired By Jndweb.com',
    'copyright' => 'Eastern-App',
    'inspired_by' => 'Inspired By <a href="https://jndweb.com" target="_blank">Jndweb</a>',
    'powered_by' => 'Powered By <a href="https://laravel.com" target="_blank">Laravel</a>',
    'them-skin' => 'blue',
    'activity_log' => env('APP_ACTIVITY_LOG', false),
];
