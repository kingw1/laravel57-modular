<?php

namespace App\Http\Controllers;

use Facuz\Theme\Contracts\Theme;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Theme instance.
     *
     * @var \Facuz\Theme\Theme
     */
    protected $theme;

    /**
     * Construct
     *
     * @return void
     */
    public function __construct(Theme $theme)
    {
        $this->theme = $theme->uses(config('theme.themeDefault'))->layout(config('theme.layoutDefault'));
    }
}
