<div class="">
    <form method="POST" class="form-horizontal">
        <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" required="required">
            <span class="fa fa-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required="required">
            <span class="fa fa-key form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">LOGIN</button>
            </div>
        </div>
    </form>
</div>
