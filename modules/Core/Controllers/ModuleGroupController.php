<?php

namespace Module\Core\Controllers;

use App\Http\Controllers\Controller;

class ModuleGroupController extends Controller
{
    public function index()
    {
        return $this->theme->of('Core.Views.ModuleGroup.index')->render();
    }
}
