<?php

namespace Module\Core\Controllers;

use App\Http\Controllers\Controller;

class ActivityLogController extends Controller
{
    public function index()
    {
        return $this->theme->of('Core.Views.ActivityLog.index')->render();
    }
}
