<?php

namespace Module\Core\Controllers;

use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function index()
    {
        $this->theme->uses('default')->layout('auth');
        return $this->theme->of('Core.Views.Auth.index')->render();
    }
}
