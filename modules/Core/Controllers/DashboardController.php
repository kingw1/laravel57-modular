<?php

namespace Module\Core\Controllers;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return $this->theme->of('Core.Views.Dashboard.index')->render();
    }
}
