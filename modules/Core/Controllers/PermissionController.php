<?php

namespace Module\Core\Controllers;

use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function index()
    {
        return $this->theme->of('Core.Views.Permission.index')->render();
    }
}
