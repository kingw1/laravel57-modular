<?php

namespace Module\Core\Controllers;

use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function index()
    {
        return $this->theme->of('Core.Views.Module.index')->render();
    }
}
