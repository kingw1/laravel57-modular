<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>@get('title')</title>

        @styles()
        @scripts('header')
    </head>

    <body class="hold-transition skin-@get('theme') sidebar-mini">
        <div class="wrapper">
            @partial('header')
            @partial('sidebar')

            <div class="content-wrapper">
                <section class="content">
                    @content()
                </section>
            </div>
        </div>
        @scripts()
        @partial('footer')
    </body>
</html>
