<?php

return array(
    'inherit' => null,
    'events' => array(
        'before' => function ($theme) {
            $theme->setTitle(config('web.title'));
            $theme->setAuthor(config('web.author'));
            $theme->setKeywords(config('web.keywords'));
            $theme->setDescription(config('web.description'));
            $theme->setTheme(config('web.theme-skin'));
        },
        'asset' => function ($asset) {
            // $asset->themePath()->add([
            //     ['style', 'css/style.css'],
            //     ['script', 'js/script.js'],
            // ]);
        },
        'beforeRenderTheme' => function ($theme) {
            $theme->asset()->add('app-css', 'css/app.css');
            $theme->asset()->container('header')->add('app-js', 'js/app.js');

        },
        'beforeRenderLayout' => array(
            //
        ),
    ),
);
