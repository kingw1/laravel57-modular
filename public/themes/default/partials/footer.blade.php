<footer class="main-footer">
    <div class="pull-left">
        &copy; {{ date('Y') }} {{ config('web.copyright') }}
    </div>
    <div class="pull-right">
        <small>{!! config('web.inspired_by') !!}</small>
        <small>{!! config('web.powered_by') !!}</small>
    </div>
</footer>
