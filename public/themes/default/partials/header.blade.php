<header class="main-header">
    <a class="logo">
        <div class="logo-lg">
            @if (isset($setting) && is_object($setting))
            <img src="{{ asset("storage/images/$setting->logo") }}" style="margin-right:5px;width:50px;"/>
            @endif
            {{ config('web.name') }}
        </div>
        <div class="logo-mini">
            @if (isset($setting) && is_object($setting))
            <img src="{{ asset("storage/images/$setting->logo") }}" style="width:30px;"/>
            @endif
        </div>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <a class="sidebar-toggle" data-toggle="offcanvas" href="#" role="button">
            <span class="sr-only">
                Toggle navigation
            </span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" />
                        <img alt="User Image" class="user-image" src="{{ asset('img\avatar.png') }}">
                        <span class="hidden-xs">
                            {{ auth()->user()->name }}
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ asset('img\avatar.png') }}" class="img-circle" alt="User Image" />
                            <p>
                                {{ auth()->user()->name }}
                                <small></small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('profile.index', auth()->user()->id) }}" class="btn btn-default btn-flat">@lang ('profile.profile')</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('password.index', auth()->user()->id) }}" class="btn btn-default btn-flat">@lang ('password.password')</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/logout"><i class="fa fa-sign-out"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
