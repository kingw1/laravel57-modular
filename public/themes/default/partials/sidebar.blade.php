@php
// $coreMenus = app(\Module\admin\module\models\Module::class)->coreMenus();
$coreMenus = [];
@endphp

<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            @if (count($coreMenus))
            @foreach ($coreMenus as $coreMenu)
            <li class="treeview {{ request()->segment(2) == $coreMenu->alias ? 'active' : '' }}">
                <a href="/backend/{{ $coreMenu->alias }}">
                    <i class="fa {{ $coreMenu->icon }}"></i>
                    <span>@lang("$coreMenu->permission")</span>
                </a>

                {{-- Submodules --}}
                @php
                $subModules = $coreMenu->subMenus();
                @endphp

                @if (count($subModules))
                <ul class="treeview-menu">
                    @foreach ($subModules as $subModule)
                        <li class="{{ request()->segment(2) == $coreMenu->alias && request()->segment(3) == $subModule->alias ? 'active' : '' }}">
                            <a href="/backend/{{ route($subModule->permission) }}">
                                <i class="fa fa-circle-o"></i> {{ trans("$subModule->permission") }}
                            </a>
                        </li>
                    @endforeach
                </ul>
                @endif
                {{-- // Submodules --}}
            </li>
            @endforeach
            @endif
        </ul>
    </section>
</aside>
